# bgm_downloader

#### Description
An app that obtains animation resources. Through this App, you can search for resource links of the animation works being shown & completed.

#### Installation

1.  Android
Download the provided apk installation package and install it. (Need to allow installation of applications from third-party sources)
2.  iOS
[Please install according to the linked page](http://b.mstat.top/forum/?tid=13) (The iOS version is no longer available from the rc7 version and the 1.0.2+1 official version).

#### Instructions
Only for learning and communication.
